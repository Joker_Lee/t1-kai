/**
 * @Class: TieuThucDaoImpl
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ffse1704.dao.TieuThucDao;
import com.ffse1704.dao.repository.RepositoryDaoImpl;
import com.ffse1704.model.TieuThuc;

@Repository
public class TieuThucDaoImpl extends RepositoryDaoImpl<TieuThuc, String> implements TieuThucDao {

	/*
	 * listTieuThuc
	 * 
	 * @see ffse1704.dao.tieuthuc.TieuThucDao#listTieuThuc(int, int)
	 */

	@Override
	public List<TieuThuc> listTieuThuc(int iDisplayStart, int iDisplayLength) {
		List<TieuThuc> TieuThucList = createQuery("from TieuThuc tt where tt.maCha = '" + "'")
				.setFirstResult(iDisplayStart).setMaxResults(iDisplayLength).list();
		return TieuThucList;
	}

	/*
	 * getRecordsTotalMaChaNull
	 * 
	 * @see ffse1704.dao.tieuthuc.TieuThucDao#getRecordsTotal()
	 */
	@Override
	public int getRecordsTotalMaChaNull() {

		int rowCount = createQuery("from TieuThuc tt where tt.maCha = '" + "'").list().size();
		return rowCount;
	}

	/*
	 * getListTieuThucByMaCha
	 * 
	 * @see ffse1704.dao.tieuthuc.TieuThucDao#getRecordsByIdTieuThuc(java.lang.
	 * String)
	 */

	@Override
	public List<TieuThuc> getListTieuThucByMaCha(String maCha) {
		List<TieuThuc> listTieuThucCon = createQuery("from TieuThuc  where maCha = '" + maCha + "'").list();
		return listTieuThucCon;
	}

	@Override
	public List<TieuThuc> getListTieuThucByMaChaNull() {
		List<TieuThuc> listTieuThucMaChaNull = createQuery("from TieuThuc tt where tt.maCha = '" + "'").list();
		return listTieuThucMaChaNull;
	}

}
