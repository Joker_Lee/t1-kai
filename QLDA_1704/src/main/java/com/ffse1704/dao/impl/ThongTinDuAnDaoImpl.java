/**
 * @Class: ThongTinDuAnDaoImpl
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ffse1704.dao.KhachHangDao;
import com.ffse1704.dao.NhanVienDao;
import com.ffse1704.dao.PhongBanDao;
import com.ffse1704.dao.ThongTinDuAnDao;
import com.ffse1704.dao.TieuThucDao;
import com.ffse1704.dao.repository.RepositoryDaoImpl;
import com.ffse1704.model.ThongTinDuAn;

@Repository
public class ThongTinDuAnDaoImpl extends RepositoryDaoImpl<ThongTinDuAn, String> implements ThongTinDuAnDao {

	@Autowired
	private TieuThucDao tieuThucDao;
	@Autowired
	private NhanVienDao nhanVienDao;
	@Autowired
	private KhachHangDao khachHangDao;
	@Autowired
	private PhongBanDao phongBanDao;

	@Override
	public List<ThongTinDuAn> listDuAn(String maTrangThai, int iDisplayStart, int iDisplayLength) {
		List<ThongTinDuAn> duAnList = createQuery("from ThongTinDuAn where maTrangThai ='" + maTrangThai + "'")
				.setFirstResult(iDisplayStart).setMaxResults(iDisplayLength).list();
		duAnList.forEach(da -> {
			da.setPhongBan(phongBanDao.findById(da.getMaPhongBan()));
			da.setNhanVienPm(nhanVienDao.findById(da.getMaNhanVienPM()));
			da.setTrangThai(tieuThucDao.findById(da.getMaTrangThai()));
			da.setKhachHang(khachHangDao.findById(da.getMaKhachHang()));
		});
		return duAnList;
	}

	@Override
	public int getRecordsTotal(String maTrangThai) {
		int rowCount = createQuery("from ThongTinDuAn where maTrangThai ='" + maTrangThai + "'").list().size();
		return rowCount;
	}

	@Override
	public int getRecordsByIdDuAn(String maDuAn) {
		int rowCount = createQuery("from ThongTinDuAn where maDuAn='" + maDuAn + "'").list().size();
		return rowCount;
	}

	@Override
	public ThongTinDuAn viewOneDuAn(String maDuAn) {
		List<ThongTinDuAn> duAnList = createQuery("from ThongTinDuAn where maDuAn ='" + maDuAn + "'").list();
		duAnList.forEach(da -> {
			da.setPhongBan(phongBanDao.findById(da.getMaPhongBan()));
			da.setNhanVienPm(nhanVienDao.findById(da.getMaNhanVienPM()));
			da.setTrangThai(tieuThucDao.findById(da.getMaTrangThai()));
			da.setKhachHang(khachHangDao.findById(da.getMaKhachHang()));
		});
		ThongTinDuAn duAn = duAnList.get(0);
		return duAn;
	}

}
