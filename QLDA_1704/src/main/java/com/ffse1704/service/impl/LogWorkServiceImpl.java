package com.ffse1704.service.impl;

import java.text.DecimalFormat;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ffse1704.dao.LogWorkDao;
import com.ffse1704.model.CongViecDuAn;
import com.ffse1704.model.LogWork;
import com.ffse1704.service.LogWorkService;

@Service
public class LogWorkServiceImpl implements LogWorkService {

	private LogWorkDao logWorkDao;

	public LogWorkDao getLogWorkDao() {
		return logWorkDao;
	}

	@Autowired
	public void setLogWorkDao(LogWorkDao logWorkDao) {
		this.logWorkDao = logWorkDao;
	}

	@Override
	@Transactional
	public boolean addNew(LogWork logwork) {
		boolean flag = false;
		try {
			this.logWorkDao.save(logwork);
			flag = true;
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	@Override
	@Transactional
	public boolean update(LogWork logwork) {
		boolean flag = false;
		try {
			this.logWorkDao.update(logwork);
			flag = true;
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	@Override
	@Transactional
	public LogWork getOneLogWork(int id) {
		return this.logWorkDao.getOneLogWork(id);
	}

	@Override
	@Transactional
	public List<LogWork> listLogWorkPage(String maDuAn, String maCongViec, int iDisplayStart, int iDisplayLength) {
		return this.logWorkDao.listLogWorkPage(maDuAn, maCongViec, iDisplayStart, iDisplayLength);
	}

	@Override
	@Transactional
	public int getRecordsTotal(String maDuAn, String maCongViec) {
		return this.logWorkDao.getRecordsTotal(maDuAn, maCongViec);
	}

	@Override
	@Transactional
	public List<LogWork> listLogWork(String maDuAn, String maCongViec) {
		return this.logWorkDao.listLogWork(maDuAn, maCongViec);
	}
	
	@Override
	@Transactional
	public double[] handleTmLogWrk(List<LogWork> listAll, CongViecDuAn congViecDuAn) {
		double timeLogWork = 0;
		double timeRemainning = 0;
		double persentLogWork = 0;
		
		long diff = congViecDuAn.getThoiGianDong().getTime() - congViecDuAn.getThoiGianMo().getTime();
		double timeAll = diff / (60 * 60 * 1000) % 24;
		for (int i = 0; i < listAll.size(); i++) {
			timeLogWork += listAll.get(i).getThoiGian();
		}
		double a = timeLogWork / timeAll;
		timeRemainning = (timeAll - timeLogWork);

		DecimalFormat df = new DecimalFormat("0.0");
		String str = df.format(timeRemainning);
		double result = Double.valueOf(str);

		persentLogWork = (int) (a * 100);
		
		double[] returnPrLogWrk = {timeAll, persentLogWork, timeLogWork, result};
		
		return returnPrLogWrk;
	}
}
