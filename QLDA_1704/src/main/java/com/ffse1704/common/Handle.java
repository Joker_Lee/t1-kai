/**
 * 
 * @Class: Handle
 * 
 * @author NhatLKH
 * 
 **/
package com.ffse1704.common;

public class Handle {

	/**
	 *  pagination
	 * 
	 * @param page
	 * @param allRecord
	 * @param recordInpage
	 * @return int[]
	 */
	public static int[] pagination(Integer page, Integer allRecord, Integer recordInpage) {
		int allItem = allRecord;
		int reCordInPage = recordInpage;
		double pageTotal = allItem / reCordInPage + ((allItem % reCordInPage) == 0 ? 0 : 1);
		int total = (int) pageTotal;
		int start = (page * (int) reCordInPage) - (int) reCordInPage;
		int[] returnPr = { total, start };
		return returnPr;
	}

}
