/**
 * @Controller KhachHangController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ffse1704.common.Handle;
import com.ffse1704.common.Utils;
import com.ffse1704.model.KhachHang;
import com.ffse1704.service.KhachHangService;

@Controller
@RequestMapping("/khachhang")
public class KhachHangController {

	@Autowired
	private KhachHangService khachHangService;

	/**
	 * list Customer
	 * 
	 * @param page
	 *            ( current page )
	 * @param model
	 *            ( sent parameters to view )
	 * @return List Customer screen with pagination
	 * @throws SQLException
	 */
	@RequestMapping("/{page}")
	public String list(@PathVariable int page, Model model) throws SQLException {
		int allItem = khachHangService.getRecordsTotal();
		int reCordInPage = 2;
		int[] pagination = Handle.pagination(page, allItem,
				reCordInPage);/*
								 * get total record, start record in page, record
								 * in page
								 */
		List<KhachHang> list = khachHangService.listKhachHang(pagination[1], reCordInPage);
		model.addAttribute("listKhachHang", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		return "khachhang/list";
	}

	/**
	 * create one Customer
	 * 
	 * @return Customer Object
	 */
	@RequestMapping("/addkhachhang")
	public ModelAndView viewAddDatabase() {
		return new ModelAndView("khachhang/add", "command", new KhachHang());
	}

	/**
	 * action save Customer
	 * 
	 * @param khachHang
	 *            (parameters of Customer object from the form )
	 * @param result
	 *            ( check argument )
	 * @return Object Customer or Mess error
	 */
	@RequestMapping(value = "/savekhachhang", method = RequestMethod.POST)
	public String addNewKhachHang(@ModelAttribute("command") @Valid KhachHang khachHang, BindingResult result) {
		if (result.hasErrors()) {
			return "khachhang/add";
		} else {
			String idCustomer = Utils.randomAlphaNumeric(10);
			int searchKhachHang = khachHangService.getRecordsByIdPKhachHang(idCustomer);
			while (searchKhachHang != 0) {
				idCustomer = Utils.randomAlphaNumeric(10);
				searchKhachHang = khachHangService.getRecordsByIdPKhachHang(idCustomer);
			}
			khachHang.setMaKhachHang(idCustomer);
			khachHangService.addNew(khachHang);
		}	
		return "redirect:/khachhang/1";
	}

	/**
	 * open edit Customer screen
	 * 
	 * @param idKhachHang
	 * @param model
	 *            (Customer object)
	 * @return Customer object and edit customer screen
	 */
	@RequestMapping(value = "/editkhachhang/{idKhachHang}")
	public String viewEditKhachHang(@PathVariable String idKhachHang, Model model) {
		KhachHang khachHang = khachHangService.getById(idKhachHang);
		model.addAttribute("command", khachHang);
		return "khachhang/edit";
	}

	/**
	 * action save new data of Customer
	 * 
	 * @param khachHang
	 * @param result  ( check argument )
	 * @return Start page list Customer screen or Mess valid error
	 */
	@RequestMapping(value = "/editsavekhachhang", method = RequestMethod.POST)
	public String editSaveKhachHang(@ModelAttribute("command") @Valid KhachHang khachHang, BindingResult result) {
		if (result.hasErrors()) {
			return "khachhang/edit";
		} else {
			khachHangService.update(khachHang);
		}

		return "redirect:/khachhang/1";
	}

	/*
	 * delete KhachHang# get KhachHang by id# delete KhachHang by id
	 */
	/**
	 * open delete Customer screen
	 * 
	 * @param idKhachHang
	 * @param model (Customer object)
	 * @return view: khachhang/delete
	 */
	@RequestMapping(value = "/deletekhachhang/{idKhachHang}")
	public String viewDeleteKhachHang(@PathVariable String idKhachHang, Model model) {
		KhachHang khachHang = khachHangService.getById(idKhachHang);
		model.addAttribute("command", khachHang);
		return "khachhang/delete";
	}

	/**
	 * action delete Customer
	 * 
	 * @param maKH
	 * @param redirect
	 * @return Mess success or error
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteKhachHang(@RequestParam("maKhachHang") String maKH, RedirectAttributes redirect) {
		KhachHang khachHang = khachHangService.getById(maKH);
		boolean result = khachHangService.delete(khachHang);
		String msg = null;
		if (result) {
			msg = "Đã xóa " + khachHang.getTenKhachHang() + " thành công!";
		} else {
			msg = "Đã xóa " + khachHang.getTenKhachHang() + " thất bại!";
		}
		redirect.addFlashAttribute("msg", msg);
		return "redirect:/khachhang/1";
	}

}