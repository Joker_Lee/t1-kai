/**
 * @Controller: CongViecDuAnController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ffse1704.common.Handle;
import com.ffse1704.model.CongViecDuAn;
import com.ffse1704.service.CongViecDuAnService;

@Controller
@RequestMapping("/congviecduan")
public class CongViecDuAnController {

	@Autowired
	private CongViecDuAnService congViecDuAnService;

	/* list Cong viec */
	/**
	 * @param maDuAn
	 * @param page
	 * @param model
	 * @return Object
	 * @throws SQLException
	 */
	@RequestMapping("/list")
	public String list(@RequestParam("maDuAn") String maDuAn, @RequestParam("page") Integer page, Model model)
			throws SQLException {
		/*pagination*/
		int allItem = congViecDuAnService.getRecordsTotalByMaCha(maDuAn);
		int reCordInPage = 2;
		int[] pagination = Handle.pagination(page, allItem, reCordInPage);/*get total record, start record in page, record in page*/
		List<CongViecDuAn> list = congViecDuAnService.listCongViecDuAnByMaCha(maDuAn, pagination[1], reCordInPage);
		/*end pagiation*/
		
		model.addAttribute("tenDuAn", list.get(0));
		model.addAttribute("list", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		/*
		 * CongViecDuAn.TrangThai test = CongViecDuAn.TrangThai.Open;
		 * model.addAttribute("test", test);
		 */
		return "congviecduan/list";
	}


	/**
	 * @param id
	 * @param model
	 * @return Object
	 * @throws ParseException
	 */
	@RequestMapping("")
	public String getOne(@RequestParam("id") Integer id, Model model) throws ParseException {
		CongViecDuAn congViecDuAn = congViecDuAnService.getCongViecDuAnById(id);
		model.addAttribute("view", congViecDuAn);
		//in milliseconds
		long diff = congViecDuAn.getThoiGianDong().getTime() - congViecDuAn.getThoiGianMo().getTime();
		long diffHours = diff / (60 * 60 * 1000) % 24;
		model.addAttribute("timeAll", diffHours);
		return "congviecduan/viewone";
	}

}
