/**
 * @Controller HomeController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	/**
	 * @return Home screen
	 */
	@RequestMapping("/")
	public String viewHome() {
		return "home";
	}

}
