/**
 * @Controller VatLieuController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ffse1704.common.Handle;
import com.ffse1704.model.VatLieu;
import com.ffse1704.model.valid.VatLieuAddValidator;
import com.ffse1704.service.VatLieuService;

@Controller
@RequestMapping("/vatlieu")
public class VatLieuController {
	@Autowired
	private VatLieuService vatLieuService;

	@Autowired
	private VatLieuAddValidator vatLieuAddValidator;

	@InitBinder
	public void initValidVatLieu(WebDataBinder binder) {
		binder.addValidators(vatLieuAddValidator);
	}

	/**
	 * list material
	 * @param page ( current page )
	 * @param order ( orderby asc or desc parameter)
	 * @param model ( sent parameters to view )
	 * @return view: VatLieu/list
	 * @throws SQLException
	 */
	@RequestMapping("/list")
	public String list(@RequestParam(value = "page", required = true) Integer page,
			@RequestParam(value = "order", required = false) String order, Model model) throws SQLException {
		int allItem = vatLieuService.getRecordsTotal();
		int reCordInPage = 2;
		int[] pagination = Handle.pagination(page, allItem, reCordInPage);/* get total record, start record in page, record in page*/
		String ord = null;
		if (order != null && order.equals("desc")) {
			ord = "desc";
		} else {
			ord = "asc";
		}

		List<VatLieu> list = vatLieuService.view(pagination[1], reCordInPage, ord);
		model.addAttribute("list", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		return "VatLieu/list";
	}

	/**
	 * open create material form
	 * @param model( Material object )
	 * @return view: VatLieu/add_form
	 */
	@RequestMapping(value = { "/add_form" })
	public String openCreateForm(Model model) {
		model.addAttribute("vatlieu", new VatLieu());
		return "VatLieu/add_form";
	}

	/**
	 * create action
	 * @param vatLieu ( new material )
	 * @param result ( check valid form )
	 * @param redirectAttributes ( flash mess )
	 * @return view and mess
	 */
	@RequestMapping(value = { "/create" }, method = RequestMethod.POST)
	public String create(@ModelAttribute("vatlieu") @Valid VatLieu vatLieu, BindingResult result,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "VatLieu/add_form";
		} else {
			boolean result1 = vatLieuService.addNew(vatLieu);
			String mess = null;
			if (result1) {
				mess = "Thành Công";
				redirectAttributes.addFlashAttribute("mess", mess);
				return "redirect:/vatlieu/list?page=1&order=desc";
			} else {
				mess = "Thất bại";
				redirectAttributes.addFlashAttribute("mess", mess);
				return "VatLieu/add_form";
			}
		}
	}

	/**
	 * remove material
	 * @param maCongViec ( id work )
	 * @param redirectAttributes ( flash mess )
	 * @return view: redirect:/vatlieu/list
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam("maCongViec") String maCongViec,RedirectAttributes redirectAttributes)
			throws IllegalStateException, IOException {		
		VatLieu vl =vatLieuService.delete(maCongViec);
		String mess=null;
		if(vl!=null) {
			 mess="Xoá thành công";
		}else {
			 mess="Xoá thất bại";
		}
		redirectAttributes.addFlashAttribute("mess", mess);
		return "redirect:/vatlieu/list?page=1&order=desc";
	}

	/**
	 * open material information change form
	 * @param maCongViec ( id work )
	 * @param model ( sent parameters to view )
	 * @return view: VatLieu/update
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping("/edit/{maCongViec}")
	public String showFormUpdate(@PathVariable("maCongViec") String maCongViec, Model model)
			throws IllegalStateException, IOException {
		VatLieu vl = vatLieuService.getId(maCongViec);
		model.addAttribute("command", vl);
		model.addAttribute("maCongViec", vl.getMaCongViec());
		return "VatLieu/update";
	}

	/**
	 * change action
	 * @param vl ( new parameters of material )
	 * @param result ( check valid )
	 * @param redirectAttributes
	 * @return view
	 */
	@RequestMapping("/update")
	public String update(@ModelAttribute("vatlieu") @Valid VatLieu vl, BindingResult result,
			RedirectAttributes redirectAttributes) {
		Boolean result1 = vatLieuService.update(vl);
		String mess = null;
		if (result1 == true) {
			mess = "Thành Công";
			redirectAttributes.addFlashAttribute("mess", mess);
			return "redirect:/vatlieu/list?page=1&order=desc";
		} else {
			mess = "Thất bại";
			redirectAttributes.addFlashAttribute("mess", mess);
			return "VatLieu/add_form";
		}
	}

}
