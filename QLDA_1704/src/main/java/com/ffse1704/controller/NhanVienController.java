/**
 * @Controller NhanVIenController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ffse1704.common.Handle;
import com.ffse1704.model.NhanVien;
import com.ffse1704.service.NhanVienService;

@Controller
@RequestMapping("/nhanvien")
public class NhanVienController {

	@Autowired
	private NhanVienService nhanVienService;

	/**
	 * list staff screen
	 * @param page ( current page )
	 * @param model ( sent parameters to view )
	 * @return view: nhanvien/list
	 * @throws SQLException
	 */
	@RequestMapping("/{page}")
	public String list(@PathVariable int page, Model model) throws SQLException {
		int allItem = nhanVienService.getRecordsTotal();
		int reCordInPage = 2;
		int[] pagination = Handle.pagination(page, allItem, reCordInPage);/* get total record, start record in page, record in page*/
		List<NhanVien> list = nhanVienService.listNhanVien(pagination[1], reCordInPage);
		model.addAttribute("list", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		return "nhanvien/list";
	}

	/**
	 * get profile staff
	 * @param maNhanVien (id staff)
	 * @param model ( sent parameters to view )
	 * @return view: nhanvien/view
	 */
	@RequestMapping("/viewNhanVien/{maNhanVien}")
	public String viewOne(@PathVariable String maNhanVien, Model model) {
		model.addAttribute("viewOne", nhanVienService.getOneNhanVien(maNhanVien));
		return "nhanvien/view";
	}

	/**
	 * edit profile staff
	 * @param maNhanVien (id staff)
	 * @param model ( sent parameters to view )
	 * @return view: nhanvien/edi
	 */
	@RequestMapping("/editNhanVien/{maNhanVien}")
	public String editOne(@PathVariable String maNhanVien, Model model) {
		model.addAttribute("editOne", nhanVienService.getOneNhanVien(maNhanVien));
		return "nhanvien/edit";
	}
}
