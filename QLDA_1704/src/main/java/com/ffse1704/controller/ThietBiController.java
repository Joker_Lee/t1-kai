/**
 * @Controller ThietBiController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ffse1704.model.ThietBi;
import com.ffse1704.model.valid.ThietBiAddValidator;
import com.ffse1704.service.ThietBiService;

@Controller
@RequestMapping("/thietbi")
public class ThietBiController {

	@Autowired
	private ThietBiService thietBiService;
	
	@Autowired
	private ThietBiAddValidator thietBiVaid;
	
	@InitBinder
	public void initValidThietBi(WebDataBinder binder) {
		binder.addValidators(thietBiVaid);
	}

	/**
	 * list Device in project
	 * @param maDA ( id project )
	 * @param model ( sent parameters to view )
	 * @return view: thietbi/list
	 */
	@RequestMapping("/{maDA}")
	public String list(@PathVariable String maDA, Model model) {
		model.addAttribute("viewOne", this.thietBiService.viewOne(maDA));
		model.addAttribute("maDA", maDA);
		return "thietbi/list";

	}

	/**
	 * open create Device form
	 * @param model ( sent parameters to view )
	 * @param maDA ( id project)
	 * @return view: thietbi/add
	 */
	@RequestMapping(value = "/add/{maDA}", method = RequestMethod.GET)
	public String showFormAdd(Model model, @PathVariable String maDA) {
		ThietBi tb = new ThietBi();
		tb.setMaDuAn(maDA);
		model.addAttribute("command", tb);
		return "thietbi/add";
	}

	/**
	 * Open the device information change form
	 * @param id ( id Device )
	 * @param model ( sent parameters to view )
	 * @param tb ( Device object )
	 * @return view: thietbi/update
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping("/edit/{id}")
	public String showFormUpdate(@PathVariable("id") int id, Model model, ThietBi tb)
			throws IllegalStateException, IOException {
		tb = thietBiService.getThietBiById(id);
		model.addAttribute("command", tb);
		model.addAttribute("id", tb.getId());
		return "thietbi/update";
	}

	/**
	 * save action
	 * @param tb ( Device object )
	 * @param result ( check valid form )
	 * @return view
	 */
	@RequestMapping("/save")
	public String saveThietBi(@ModelAttribute("command") @Valid ThietBi tb, BindingResult result) {
		if (result.hasErrors()) {
			return "thietbi/add";
		}
		thietBiService.addThietBi(tb);
		return "redirect:/thietbi/" + tb.getMaDuAn();
	}

	/**
	 * update action
	 * @param tb ( Device object )
	 * @param result ( check valid form)
	 * @return view
	 */
	@RequestMapping("/update")
	public String updateThietBi(@ModelAttribute("command") @Valid ThietBi tb, BindingResult result) {
		if (result.hasErrors()) {
			return "thietbi/update";
		}
		thietBiService.updateThietBi(tb);
		return "redirect:/thietbi/" + tb.getMaDuAn();
	}

	/**
	 * remove device
	 * @param id ( id Device)
	 * @return view: redirect:/thietbi/idDevice
	 */
	@RequestMapping(value="/remove", method = RequestMethod.POST)
	public String removeThietBi(@RequestParam("id") Integer id) {
		ThietBi tb = thietBiService.getThietBiById(id);
		thietBiService.removeThietBi(tb);
		return "redirect:/thietbi/" + tb.getMaDuAn();
	}

}
