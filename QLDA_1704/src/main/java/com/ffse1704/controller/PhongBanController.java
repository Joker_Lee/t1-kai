/**
 * @Controller PhongBanController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ffse1704.common.Constants;
import com.ffse1704.common.Handle;
import com.ffse1704.model.PhongBan;
import com.ffse1704.model.TieuThuc;
import com.ffse1704.service.PhongBanService;
import com.ffse1704.service.TieuThucService;

//import static com.ffse1704.common.Constants.deflaut_status_department;

@Controller
@RequestMapping("/phongban")
public class PhongBanController {
	
	@Autowired
	private PhongBanService phongBanService;
	
	@Autowired
	private TieuThucService tieuThucService;

	/**
	 * get list Department
	 * @param trangThai (status Department)
	 * @param page ( current page )
	 * @param model ( sent parameters to view )
	 * @return view: phongban/list
	 * @throws SQLException
	 */
	@RequestMapping("/{trangThai}&{page}")
	public String list(@PathVariable String trangThai, @PathVariable int page, Model model) throws SQLException {
		int allItem = phongBanService.getRecordsTotal(trangThai);
		int reCordInPage = 2;
		int[] pagination = Handle.pagination(page, allItem, reCordInPage);/* get total record, start record in page, record in page*/
		List<PhongBan> list = phongBanService.listPhongBan(trangThai,pagination[1], reCordInPage);
		model.addAttribute("list", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		return "phongban/list";
	}

	/**
	 * open add Department screen
	 * @return Department object
	 */
	@RequestMapping("/addphongban")
	public ModelAndView viewAdd() {
		return new ModelAndView("phongban/add", "command", new PhongBan());
	}

	/**
	 * save action
	 * @param phongBan (parameters of Department object )
	 * @param result ( check valid form )
	 * @param model ( sent flash mess to form  )
	 * @return view and mess
	 */
	@RequestMapping(value = "/savephongban", method = RequestMethod.POST)
	public String createDepartment(@ModelAttribute("command") @Valid PhongBan phongBan, BindingResult result,Model model) {
		if (result.hasErrors()) {
			return "phongban/add";
		} else {
			int searchPhongBan = phongBanService.getRecordsByIdPhongBan(phongBan.getMaPhongBan());
			if(searchPhongBan==0) {
				phongBan.setTrangThai(Constants.deflaut_status_department);
				phongBanService.addNew(phongBan);
			}else {
				String mess = "Mã phòng ban tồn tại";
				model.addAttribute("mess", mess);
				return "phongban/add";
			}
			
		}

		return "redirect:/phongban/TTPB1&1";// will redirect to viewemp request mapping
	}
	
	/** 
	 * open edit Department form
	 * @param idPhongBan ( id Department )
	 * @param model ( sent parameters to view )
	 * @return view: phongban/edit
	 */
	@RequestMapping(value = "/editphongban/{idPhongBan}")
	public String viewEditPhongBan(@PathVariable String idPhongBan, Model model) {
		List<TieuThuc> listTieuThuc = tieuThucService.getListTieuThucByMaCha("TTPB");
		PhongBan phongBan = phongBanService.getPhongBanbyIdPhongBan(idPhongBan);
		model.addAttribute("command", phongBan);
		model.addAttribute("listTrangThai", listTieuThuc);
		return "phongban/edit";
	}
	
	/**
	 * edit action
	 * @param phongBan ( new parameters of Department )
	 * @param result ( check valid form )
	 * @return redirect:/phongban/TTPB1&1
	 */
	@RequestMapping(value = "/editsavephongban", method = RequestMethod.POST)
	public String editSave(@ModelAttribute("command") @Valid PhongBan phongBan, BindingResult result) {
		if (result.hasErrors()) {
			return "phongban/edit";
		} else {
			phongBanService.addNew(phongBan);
		}
		return "redirect:/phongban/TTPB1&1";// will redirect to viewemp request mapping
	}
	
	
	/*@RequestMapping(value = "/deletephongban/{idPhongBan}")
	public String viewDeletePhongBan(@PathVariable String idPhongBan, Model model) {
		PhongBan phongBan = phongBanService.getPhongBanbyIdPhongBan(idPhongBan);
		model.addAttribute("command", phongBan);
		return "phongban/delete";
	}
	
	@RequestMapping(value = "/submitdeletephongban", method = RequestMethod.POST)
	public String deletePhongBan(@RequestParam("maPhongBan") String maPB, RedirectAttributes redirect) {
		PhongBan phongBan = phongBanService.getPhongBanbyIdPhongBan(maPB);
		boolean result = phongBanService.delete(phongBan);
		String msg = null;
		if (result) {
			msg = "Đã xóa " + phongBan.getTenPhongBan() + " thành công!";
		} else {
			msg = "Đã xóa " + phongBan.getTenPhongBan() + " thất bại!";
		}
		redirect.addFlashAttribute("msg", msg);
		return "redirect:/phongban/1";
	}*/
}
