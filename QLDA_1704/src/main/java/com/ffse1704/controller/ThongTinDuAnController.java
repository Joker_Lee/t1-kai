/**
 * @Controller ThongTinDuAnController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ffse1704.common.Handle;
import com.ffse1704.model.ThongTinDuAn;
import com.ffse1704.service.ThongTinDuAnService;

@Controller
@RequestMapping("/duan")
public class ThongTinDuAnController {

	@Autowired
	private ThongTinDuAnService thongTinDuAnService;

	/**
	 * list project
	 * @param trangThai ( project status )
	 * @param page ( current page )
	 * @param model ( sent parameters to view )
	 * @return view: thongtinduan/list
	 * @throws SQLException
	 */
	@RequestMapping("/{trangThai}&{page}")
	public String list(@PathVariable String trangThai, @PathVariable int page, Model model) throws SQLException {
		int allItem = thongTinDuAnService.getRecordsTotal(trangThai);
		int reCordInPage = 2;
		int[] pagination = Handle.pagination(page, allItem, reCordInPage);/* get total record, start record in page, record in page*/
		List<ThongTinDuAn> list = thongTinDuAnService.listDuAn(trangThai, pagination[1], reCordInPage);
		model.addAttribute("list", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		return "thongtinduan/list";
	}

	/**
	 * open project information
	 * @param idDuAn ( id project )
	 * @param model ( sent parameters to view )
	 * @return view: thongtinduan/view
	 */
	@RequestMapping(value = "/view/{idDuAn}")
	public String viewThongTinDuAn(@PathVariable String idDuAn, Model model) {
		// List<TieuThuc> listTieuThuc =
		// tieuThucService.getListTieuThucByMaCha("TTPB");
		ThongTinDuAn duAn = thongTinDuAnService.viewOneDuAn(idDuAn);
		model.addAttribute("command", duAn);
		// model.addAttribute("list", listTieuThuc);
		return "thongtinduan/view";
	}

}
