/**
 * @Controller LogWorkController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ffse1704.common.Handle;
import com.ffse1704.model.CongViecDuAn;
import com.ffse1704.model.LogWork;
import com.ffse1704.service.CongViecDuAnService;
import com.ffse1704.service.LogWorkService;

@Controller
@RequestMapping("/logwork")
public class LogWorkController {
	@Autowired
	private LogWorkService logWorkService;
	@Autowired
	private CongViecDuAnService congViecDuAnService;

	/**
	 * list logwork of project
	 * @param maDuAn ( id project )
	 * @param maCongViec ( id task )
	 * @param checkTime (Current time)
	 * @param page ( current page )
	 * @param model (Task object)
	 * @return view: "logwork/list"
	 * @throws SQLException
	 */
	@RequestMapping("/list")
	public String list(@RequestParam("maDuAn") String maDuAn, @RequestParam("maCongViec") String maCongViec,
			@RequestParam("checkTime") Integer checkTime, @RequestParam("page") Integer page, Model model)
			throws SQLException {
		/*pagination*/
		Integer allItem = logWorkService.getRecordsTotal(maDuAn, maCongViec);
		Integer recordInPage = 5;
		int[] pagination = Handle.pagination(page, allItem, recordInPage);/*get total record, start record in page, record in page*/
		List<LogWork> list = logWorkService.listLogWorkPage(maDuAn, maCongViec, pagination[1], recordInPage);
		/*end pagiation*/
		
		/* handle time logwork */
		List<LogWork> listAll = logWorkService.listLogWork(maDuAn, maCongViec);
		CongViecDuAn congViecDuAn = congViecDuAnService.getCongViecDuAnById(checkTime);		
		double[] returnPrLogWrk = logWorkService.handleTmLogWrk(listAll, congViecDuAn);

		model.addAttribute("timeAll", returnPrLogWrk[0]);
		model.addAttribute("persentTime", returnPrLogWrk[1]);
		model.addAttribute("timeLogWork", returnPrLogWrk[2]);
		model.addAttribute("timeRemainning", returnPrLogWrk[3]);
		/* end handle time logwork */
		
		model.addAttribute("checkOne", list.get(0));
		model.addAttribute("checkTime", checkTime);
		model.addAttribute("list", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		return "logwork/list";
	}

	/**
	 * create new logwork
	 * @param maDuAn ( id project )
	 * @param maCongViec ( id work)
	 * @param checkTime (Current time)
	 * @param model (Task object)
	 * @return view: logwork/add
	 */
	@RequestMapping("/newlogwork")
	public ModelAndView viewAdd(@RequestParam("maDuAn") String maDuAn, @RequestParam("maCongViec") String maCongViec,
			@RequestParam("checkTime") Integer checkTime, Model model) {
		/* handle time logwork */
		List<LogWork> listAll = logWorkService.listLogWork(maDuAn, maCongViec);
		CongViecDuAn congViecDuAn = congViecDuAnService.getCongViecDuAnById(checkTime);		
		double[] returnPrLogWrk = logWorkService.handleTmLogWrk(listAll, congViecDuAn);

		model.addAttribute("timeAll", returnPrLogWrk[0]);
		model.addAttribute("persentTime", returnPrLogWrk[1]);
		model.addAttribute("timeLogWork", returnPrLogWrk[2]);
		model.addAttribute("timeRemainning", returnPrLogWrk[3]);
		/* end handle time logwork */
		
		model.addAttribute("checkTime", checkTime);
		model.addAttribute("checkOne", listAll.get(0));
		
		return new ModelAndView("logwork/add", "command", new LogWork());
	}

	/**
	 * action save
	 * @param logWork
	 * @param result( check argument )
	 * @param model ( sent parameters to view )
	 * @return view
	 */
	@RequestMapping(value = "/savelogwork", method = RequestMethod.POST)
	public String submitAdd(@ModelAttribute("command") @Valid LogWork logWork, BindingResult result, Model model) {
		if (result.hasErrors()) {
			
			/* handle time logwork */
			List<LogWork> listAll = logWorkService.listLogWork(logWork.getMaDuAn(), logWork.getMaCongViec());
			CongViecDuAn congViecDuAn = congViecDuAnService.getCongViecDuAnById(logWork.getCheckTime());		
			double[] returnPrLogWrk = logWorkService.handleTmLogWrk(listAll, congViecDuAn);

			model.addAttribute("timeAll", returnPrLogWrk[0]);
			model.addAttribute("persentTime", returnPrLogWrk[1]);
			model.addAttribute("timeLogWork", returnPrLogWrk[2]);
			model.addAttribute("timeRemainning", returnPrLogWrk[3]);
			/* end handle time logwork */
			
			model.addAttribute("checkTime", logWork.getCheckTime());
			model.addAttribute("checkOne", listAll.get(0));
			return "logwork/add";
		} else {
			logWorkService.addNew(logWork);
		}
		return "redirect:/logwork/list?maDuAn=" + logWork.getMaDuAn() + "&maCongViec=" + logWork.getMaCongViec()
				+ "&checkTime=" + logWork.getCheckTime() + "&page=1";
	}

	/**
	 * open view profile logwork
	 * @param id
	 * @param maDuAn
	 * @param maCongViec
	 * @param checkTime
	 * @param model ( sent parameters to view )O
	 * @return view: logwork/edit
	 */
	@RequestMapping("/editlogwork")
	public String viewEdit(@RequestParam("id") Integer id,@RequestParam("maDuAn") String maDuAn, @RequestParam("maCongViec") String maCongViec,
			@RequestParam("checkTime") Integer checkTime, Model model) {
		/* handle time logwork */
		List<LogWork> listAll = logWorkService.listLogWork(maDuAn, maCongViec);
		LogWork getOneLogWork = logWorkService.getOneLogWork(id);
		CongViecDuAn congViecDuAn = congViecDuAnService.getCongViecDuAnById(checkTime);		
		double[] returnPrLogWrk = logWorkService.handleTmLogWrk(listAll, congViecDuAn);

		model.addAttribute("timeAll", returnPrLogWrk[0]);
		model.addAttribute("persentTime", returnPrLogWrk[1]);
		model.addAttribute("timeLogWork", returnPrLogWrk[2]);
		model.addAttribute("timeRemainning", returnPrLogWrk[3]);
		/* end handle time logwork */

		model.addAttribute("checkTime", checkTime);
		model.addAttribute("checkOne", listAll.get(0));
		model.addAttribute("command", getOneLogWork);
		
		return "logwork/edit";
	}
}
