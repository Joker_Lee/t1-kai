/**
 * @Controller TieuThucController
 * 
 * @author NhatLKH
 *
 */
package com.ffse1704.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ffse1704.common.Handle;
import com.ffse1704.model.TieuThuc;
import com.ffse1704.service.TieuThucService;

@Controller
@RequestMapping("/tieuthuc")
public class TieuThucController {
	@Autowired
	private TieuThucService tieuThucService;

	/**
	 * List The Criteria
	 * @param page (current page)
	 * @param model ( sent parameters to view )
	 * @return view: tieuthuc/list
	 * @throws SQLException
	 */
	@RequestMapping("/{page}")
	public String list(@PathVariable int page, Model model) throws SQLException {
		int allItem = tieuThucService.getRecordsTotalMaChaNull();
		int reCordInPage = 2;
		int[] pagination = Handle.pagination(page, allItem, reCordInPage);/* get total record, start record in page, record in page*/
		List<TieuThuc> list = tieuThucService.listTieuThuc(pagination[1], reCordInPage);
		model.addAttribute("list", list);
		model.addAttribute("indexPage", page);
		model.addAttribute("allPage", pagination[0]);
		return "tieuthuc/list";
	}
	
	/**
	 * open create The Criteria form
	 * @return view: tieuthuc/add and The Criteria object
	 */
	@RequestMapping("/addtieuthuc")
	public ModelAndView viewAddDatabase() {
		return new ModelAndView("tieuthuc/add", "command", new TieuThuc());
	}
	
	/**
	 * save action
	 * @param tieuThuc ( parameters of new The Criteria )
	 * @param result ( check valid form )
	 * @param model ( sent flash mess )
	 * @return view
	 */
	@RequestMapping(value = "/savetieuthuc", method = RequestMethod.POST)
	public String addNewTieuThuc(@ModelAttribute("command") @Valid TieuThuc tieuThuc, BindingResult result,Model model) {
		if (result.hasErrors()) {
			return "tieuthuc/add";
		} else {
			TieuThuc searchTieuThuc = tieuThucService.getTieuThucByIdTieuThuc(tieuThuc.getMaTieuThuc());
			if (searchTieuThuc != null) {
				String mess = "Mã tiêu thức đã tồn tại";
				model.addAttribute("mess", mess);
				return "tieuthuc/add";
			} else {
				tieuThucService.addNew(tieuThuc);
			}
		}
		return "redirect:/tieuthuc/1";
	}
	
	/**
	 * open The Criteria information change form
	 * @param idTieuThuc ( id The criteria )
	 * @param model ( sent parameters to view )
	 * @return view: tieuthuc/edit
	 */
	@RequestMapping(value = "/edittieuthuc/{idTieuThuc}")
	public String viewEditTieuThuc(@PathVariable String idTieuThuc, Model model) {
		TieuThuc tieuThuc = tieuThucService.getTieuThucByIdTieuThuc(idTieuThuc);
		model.addAttribute("command", tieuThuc);
		return "tieuthuc/edit";
	}
	
	/**
	 * edit action
	 * @param tieuThuc ( new parameters of The Criteria )
	 * @param result ( check valid )
	 * @return view
	 */
	@RequestMapping(value = "/editsavetieuthuc", method = RequestMethod.POST)
	public String editSaveTieuThuc(@ModelAttribute("command") @Valid TieuThuc tieuThuc, BindingResult result) {
		if (result.hasErrors()) {
			return "tieuthuc/edit";
		} else {
			tieuThucService.addNew(tieuThuc);
		}
		return "redirect:/tieuthuc/1";
	}
	
	/**
	 * @param idTieuThuc
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deletetieuthuc/{idTieuThuc}")
	public String viewDeleteTieuThuc(@PathVariable String idTieuThuc, Model model) {
		TieuThuc tieuThuc = tieuThucService.getTieuThucByIdTieuThuc(idTieuThuc); 
		model.addAttribute("command", tieuThuc);
		return "tieuthuc/delete";
	}

	/**
	 * @param tieuThuc
	 * @return
	 */
	@RequestMapping(value = "/submitdeletetieuthuc", method = RequestMethod.POST)
	public String deleteTieuThuc(@ModelAttribute("command") TieuThuc tieuThuc) {
		tieuThucService.delete(tieuThuc.getMaTieuThuc());;
		return "redirect:/tieuthuc/1";
	}
	
	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/")
	public String listTieuThucCha(Model model) {
		List<TieuThuc> listTieuThucCha = tieuThucService.getListTieuThucByMaChaNull();
		for (int i = 0; i < listTieuThucCha.size(); i++) {
			System.out.println(listTieuThucCha.get(i).getTenTieuThuc());
		}
		model.addAttribute("listTieuThucCha", listTieuThucCha);
		return "templates/header";
	}
}
