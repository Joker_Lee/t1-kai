<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/WEB-INF/view/templates/header.jsp" />

<div class="main-panel">
	<div class="content-wrapper">
		<div class="page-header">
			<div class="col-lg-12 stretch-card">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Customer List</h4>
						<nav aria-label="Page navigation example">
							<ul class="pagination">

								<c:if test="${indexPage > 1}">
									<li class="page-item"><a href="/khachhang/1"
										class="page-link">First</a></li>
								</c:if>


								<c:if test="${indexPage > 1}">
									<li class="page-item"><a href="/khachhang/${indexPage-1}"
										class="page-link">${indexPage-1}</a></li>
								</c:if>


								<li class="page-item"><a href="/khachhang/${indexPage}"
									class="page-link">${indexPage}</a></li>


								<c:if test="${indexPage < allPage}">
									<li class="page-item"><a href="/khachhang/${indexPage+1}"
										class="page-link">${indexPage+1}</a></li>
								</c:if>

								<c:if test="${indexPage < allPage}">
									<li class="page-item"><a href="/khachhang/${allPage}"
										class="page-link">Last</a></li>
								</c:if>

							</ul>
						</nav>
						<div role="group" aria-label="Button group with nested dropdown"
							class="btn-group float-md-right " id="add-new"
							style="margin-bottom: 10px;">
							<a href="addkhachhang" class="btn btn-info"><span
								class="fa fa-plus"></span>Create</a>
						</div>
						<div role="group" aria-label="Button group with nested dropdown"
							class="btn-group float-md-right " id="add-new"
							style="margin-bottom: 10px; float: left !important">
							<h4 style="margin-top: 20px;" class="text-success">${msg != null ? msg : ''}
								delete success!</h4>
						</div>

					</div>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Customer ID</th>
								<th>Customer name</th>
								<th>Address</th>
								<th>Phone number</th>
								<th>Email</th>
								<th>action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="emp" items="${listKhachHang}">
								<tr>
									<td>${emp.maKhachHang}</td>
									<td>${emp.tenKhachHang}</td>
									<td>${emp.diaChi}</td>
									<td>0${emp.soDienThoai}</td>
									<td>${emp.email}</td>
									<td><a href="/khachhang/editkhachhang/${emp.maKhachHang}"><span
											class="fa fa-pencil" title="change" style="padding: 5px;"></span></a>
										<a
										onclick="deleteKhachHang('${emp.maKhachHang}','${emp.tenKhachHang}')"><span
											class="fa fa-trash" title="remove"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- The Modal -->
					<div class="modal" id="modalRemove">
						<div class="modal-dialog">
							<div class="modal-content">

								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title">Are you sure ?</h4>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<!-- Modal body -->
								<div class="modal-body" id="msgNotice"></div>
								<form id="command" class="forms-sample"
									action="/khachhang/delete" method="post">
									<input id="maKhachHang" name="maKhachHang" class="form-control"
										type="hidden">
									<!-- Modal footer -->
									<div class="modal-footer">
										<button type="submit" class="btn btn-danger">Remove</button>
										<button type="button" class="btn btn-primary"
											data-dismiss="modal">Close</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- End Modal -->
				</div>
			</div>
		</div>
		<script type="text/javascript">
			deleteKhachHang = function(id, name) {
				console.log('id deleted: ' + id + ' - ' + name);
				$('#maKhachHang').val(id);
				$('#tenKhachHang').val(name);
				$('#msgNotice').text("Remove Customer: " + name + " ?");
				$('#modalRemove').modal("show");
			}
		</script>
		<jsp:include page="/WEB-INF/view/templates/footer.jsp" />