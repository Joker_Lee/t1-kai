<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:include page="/WEB-INF/view/templates/header.jsp" />

<div class="main-panel">
	<div class="content-wrapper">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Create Customer</h4>
					<div class="content-header-right ">
						<div role="group" aria-label="Button group with nested dropdown"
							class="btn-group float-md-right " id="add-new">
							<a href="<c:url value = "/khachhang/1"/>" class="btn btn-info"><span
								class="fa fa-plus"></span>Back</a>
						</div>
					</div>
					<form:form class="forms-sample" method="post"
						action="savekhachhang">
						<div class="form-group">
							<form:input path="maKhachHang" class="form-control" type="hidden" value="A"/>
						</div>
						<div class="form-group">
							<label for="exampleInputName2">Customer name</label>
							<form:input path="tenKhachHang" class="form-control" />
							<form:errors path="tenKhachHang" cssStyle="color: red" />
						</div>
						<div class="form-group">
							<label for="exampleInput3">Address</label>
							<form:input path="diaChi" class="form-control" />
							<form:errors path="diaChi" cssStyle="color: red" />
						</div>
						<div class="form-group">
							<label for="exampleInput4">Phone number</label>
							<form:input path="soDienThoai" class="form-control" />
							<form:errors path="soDienThoai" cssStyle="color: red" />
						</div>
						<div class="form-group">
							<label for="exampleInput5">Email</label>
							<form:input path="email" class="form-control" />
							<form:errors path="email" cssStyle="color: red" />
						</div>
						<div class="form-group">
							<label for="exampleInput6">Tax code</label>
							<form:input path="maSoThue" class="form-control" />
							<form:errors path="maSoThue" cssStyle="color: red" />
						</div>
						<div class="form-group">
							<label for="exampleInput7">Bank number</label>
							<form:input path="soTaiKhoan" class="form-control" />
							<form:errors path="soTaiKhoan" cssStyle="color: red" />
						</div>
						<div class="form-group">
							<label for="exampleInput8">Note</label>
							<textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
						</div>
						<button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
						<button class="btn btn-light" type="reset">Cancel</button>
					</form:form>
				</div>
			</div>
		</div>
		<jsp:include page="/WEB-INF/view/templates/footer.jsp" />