<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:include page="/WEB-INF/view/templates/header.jsp" />
<!-- datatableview -->
<%-- <link rel="stylesheet"
	href="<c:url value="/resources/css/datatableview.css"/>"> --%>
<style>
.inputModal {
	border: none; /* <-- This thing here */
	border: solid 1px #ccc;
	border-radius: 10px;
}
</style>
<div class="main-panel">
	<div class="content-wrapper">
		<div class="page-header">
			<div class="col-lg-12 stretch-card">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Department List</h4>
						<nav aria-label="Page navigation example">
							<ul class="pagination">

								<c:if test="${indexPage > 1}">
									<li class="page-item"><a href="/phongban/${trangThai}&1"
										class="page-link">First</a></li>
								</c:if>


								<c:if test="${indexPage > 1}">
									<li class="page-item"><a
										href="/phongban/${trangThai}&${indexPage-1}" class="page-link">${indexPage-1}</a></li>
								</c:if>


								<li class="page-item"><a
									href="/phongban/${trangThai}&${indexPage}" class="page-link">${indexPage}</a></li>


								<c:if test="${indexPage < allPage}">
									<li class="page-item"><a
										href="/phongban/${trangThai}&${indexPage+1}" class="page-link">${indexPage+1}</a></li>
								</c:if>

								<c:if test="${indexPage < allPage}">
									<li class="page-item"><a
										href="/phongban/${trangThai}&${allPage}" class="page-link">Last</a></li>
								</c:if>

							</ul>
						</nav>
						<div class="content-header-right ">
							<div role="group" aria-label="Button group with nested dropdown"
								class="btn-group float-md-right " id="add-new">
								<a onclick="createDepartment()" class="btn btn-info"><span
									class="fa fa-plus"></span> Create</a>
							</div>
							<div role="group" aria-label="Button group with nested dropdown"
								class="btn-group float-md-right " id="add-new"
								style="margin-bottom: 10px; float: left !important">
								<a href="<c:url value = "/phongban/TTPB1&1"/>"
									class="btn btn-gradient-dark btn-rounded btn-fw"><span
									class="fa fa-check" style="margin: 5px;"></span> Active</a> <a
									href="<c:url value = "/phongban/TTPB2&1"/>"
									class="btn btn-gradient-dark btn-rounded btn-fw"><span
									class="fa fa-times	" style="margin: 5px;"></span> Stopped</a>
								<%-- <h4 style="margin-top: 20px;" class="text-success">${msg != null ? msg : ''}
									delete success!</h4> --%>
							</div>
						</div>
						<div class="table-responsive table--no-card m-b-30">
							<table
								class="table table-hover table-borderless table-striped table-earning">
								<thead style="background-color: gray;">
									<tr>
										<th>Department ID</th>
										<th>Department name</th>
										<th>status</th>
										<th><span class="fa fa-bars"></span></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="emp" items="${list}">
										<tr>
											<td>${emp.maPhongBan}</td>
											<td>${emp.tenPhongBan}</td>
											<td>${emp.ttTrangThai.tenTieuThuc}</td>
											<td><a href="/phongban/editphongban/${emp.maPhongBan}"><span
													class="fa fa-pencil" title="change"></span></a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- The Modal -->
							<div class="modal" id="modalCreate">
								<div class="modal-dialog">
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title">Create Department</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<div style="margin: 15px;">
												<form:form class="forms-sample" id="command"
													action="/phongban/savephongban" method="post">
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">ID</label>
														<div class="col-sm-10">
															<input id="maPhongBan" name="maPhongBan"
																placeholder="Department ID" type="text"
																class="form-control inputModal">
															<form:errors path="maPhongBan" cssStyle="color: red" />
															<strong style="color: red;">${mess}</strong>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">Name</label>
														<div class="col-sm-10">
															<input id="tenPhongBan" name="tenPhongBan"
																placeholder="Department Name" type="text"
																class="form-control inputModal">
															<form:errors path="tenPhongBan" cssStyle="color: red" />
														</div>
													</div>

													<!-- Modal footer -->
													<div class="modal-footer">
														<button type="submit" class="btn btn-primary">Create</button>
														<button type="button" class="btn btn-danger"
															data-dismiss="modal">Cancel</button>
													</div>
												</form:form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		createDepartment = function() {
			console.log('Open create modal');
			$('#modalCreate').modal("show");
		}
	</script>
	<jsp:include page="/WEB-INF/view/templates/footer.jsp" />